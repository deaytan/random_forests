#!/usr/bin/env/python

##this script analyses all the pointfinder results 
##Based on amino acid or nucleotide mutations, data entries return to the blosum or substitution matrix values. 
##Generated input file includes as features all the mutations or indels, as rows all the samples. 
##Random forest algorithm performs with the generated input file and provided custom output data.
##The program provides MCC and ROC-AUC plots to evaluate prediction performances. 


import getopt, sys
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")
##read commandline arguments
fullCmdArguments = sys.argv

##further arguments
argumentList = fullCmdArguments[1:]

unixOptions = "p:o:a:y:s:r:nkh"
gnuOptions = ["path=", "output=", "antibiotic=", "ydata=", "split=", "random=", "normal", "keep", "help"]

try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	##output error, and return with an error code
	print(str(err))
	sys.exit(2)
	
def main():	

	import os
	import collections
	import re
	from Bio.SubsMat import MatrixInfo
	import numpy as np
	import random
	from sklearn.ensemble import RandomForestClassifier
	from sklearn.model_selection import StratifiedKFold
	from sklearn.metrics import matthews_corrcoef
	import matplotlib.pyplot as plt
	from sklearn.metrics import roc_curve, auc
	from scipy import interp

	##generate Blosum 62 matrix
	blosum = MatrixInfo.blosum62

	###Generate nucleotide substitution matrix 
	##transition and transversion matrix

	nucleotide = {}
	nucleotide[("A", "A")] = 1
	nucleotide[("T", "T")] = 1
	nucleotide[("G", "G")] = 1
	nucleotide[("C", "C")] = 1

	nucleotide[("A", "T")] = -3
	nucleotide[("A", "C")] = -3
	nucleotide[("A", "G")] = -3

	nucleotide[("C", "T")] = -3
	nucleotide[("G", "T")] = -3

	nucleotide[("G", "C")] = -3

	nucleotide[("A", "N")] = 0
	nucleotide[("T", "N")] = 0
	nucleotide[("G", "N")] = 0
	nucleotide[("C", "N")] = 0
	nucleotide[("N", "N")] = 0

	list_antibio = ["ethambutol","pyrazinamide","streptomycin","rifampicin","isoniazid", "all"]
	for currentArgument, currentValue in arguments:
		if currentArgument in ("-p","--path"):
			input_path = currentValue
		if currentArgument in ("-a", "--antibiotic"):
			index_anti = list_antibio.index(str(currentValue).lower())
		if currentArgument in ("-o","--output"):
			outdir = currentValue
		if currentArgument in ("-y", "--ydata"):
			y_path = currentValue
		if currentArgument in ("-h","--help"):
			print("Program options")
			print("-p --path = path to the input directory")
			print("-a --antibiotic = pick an antibiotic or select all genes")
			print("the options: ethambutol, pyrazinamide, streptomycin, rifampicin, isoniazid, all")
			print("-o --output = give a name for the output")
			print("-y --ydata = path to the true outputs")
			print("-s --split = number of CV splits")
			print("-r --random = random state")
			print("-n --normal = normalize the data")
			print("-k --keep = keep the generated ROC and MCC plots")
			print("-h --help = print the help message")
			print("Have fun!")
			sys.exit()


	##change the current directory to the directory includes Pointfinder *.tsv results
	file_dir = os.listdir("%s" % input_path)

	##be sure there is no undesired file
	list_point = []
	for each in file_dir:
		if ".sh" not in each and ".py" not in each and ".txt" not in each and ".png" not in each:
			list_point.append(each)

	##generate dictionaries, keys are genes, items are mutations
	dict_mutations = collections.defaultdict(list)
	all_samples = collections.defaultdict(list)
	all_aa = []

	for item in list_point:
		#item2 = item.split(".")[0]
		if "%s_kma_results.tsv" %item in os.listdir("%s/%s/" % (input_path, item)):
			data_tsv = open("%s/%s/%s_kma_results.tsv" % (input_path, item, item), "r")
			tsv = data_tsv.readlines()
			temp_dict = collections.defaultdict(list)
			for each in tsv[1:]:
					##split the tsv data and investigate 
					## pointfinder data basically includes three different values:
					##p indicates amino acids, n indicates nucleotides mostly in the promoter regions, r indiactes rna sequences
					##all the mutations inside these regions should be considered separetly. 
					##all the mutations and indels are scored differentl based on sequence structure.
					##amino acid sequences scored based on Blosum matrix, nucleotides including rna strains with nucleotide substitution matrix. 
					each = each.replace(" promotor", "")
					each = each.replace(" promoter", "")
					
					splitted1 = each.split("\t")[0]
					splitted = splitted1.split(" ")[0:2]
				
					if splitted[1][0] == "p":
						position = re.findall("(\-?\d+)", splitted[1])
						mutation = str(position[0]) + "_" + str(splitted[1][2]) + "_" + str(splitted[1][-1])
						
						if "ins" not in each and "del" not in each and "delins" not in each:
								dict_mutations[splitted[0].lower()].append(str(position[0]) + "_" + str(splitted[1][2]))
								temp_dict[splitted[0].lower()].append(mutation)
								all_aa.append(str(splitted[1][-1]))
						else:
								splitted2 = (each.split("\t")[1]).split("->")[0].replace(" ", "")
								splitted3 = (each.split("\t")[1]).split("->")[1].replace(" ", "")
								splitted4 = (each.split("\t")[2]).split("->")[0].replace(" ", "")
								splitted5 = (each.split("\t")[2]).split("->")[1].replace(" ", "") 
								if "*" in each:
									dict_mutations[splitted[0].lower()].append(str(position[0])+ "_" + splitted[1][2])
									temp_dict[splitted[0].lower()].append(str(position[0]))
								elif "del" in each:
									if len(position) > 1:
										i = 0
										for l in range(int(position[0]), int(position[1])+1):
											dict_mutations[splitted[0].lower()].append(str(l)+ "_" + splitted4[i])
											temp_dict[splitted[0].lower()].append(str(l))	
											i = i + 1
											

									else:
										dict_mutations[splitted[0].lower()].append(str(position[0])+ "_" + splitted[1][2])
										temp_dict[splitted[0].lower()].append(str(position[0]))
										
								elif "ins" in each:
									dict_mutations[splitted[0].lower()].append(str(position[0])+ "_" + splitted[1][2])
									temp_dict[splitted[0].lower()].append(str(position[0]))
									
								else:
									print(each)
							

					elif splitted[1][0] == "n":
						position = re.findall("(\-?\d+)", splitted[1])
						splitted2 = (each.split("\t")[1]).split("->")[0].replace(" ", "")
						splitted3 = (each.split("\t")[1]).split("->")[1].replace(" ", "")
						if "ins" not in each and "del" not in each and "delins" not in each:
							dict_mutations[splitted[0].lower()].append(str(position[0]) + "_" + str(splitted2) + "n")
							temp_dict[splitted[0].lower()].append(str(position[0]) + "_" + str(splitted2) + "_" + str(splitted3)+"n")	

						else:
							if "del" in each:
								if len(position) > 1:
									print(position)
									for l in range(int(position[0]), int(position[1])+1):
										dict_mutations[splitted[0].lower()].append(str(l) + "_" + str(splitted2) + "n")##wild type
										temp_dict[splitted[0].lower()].append(str(l))
										
								else:
									dict_mutations[splitted[0].lower()].append(str(position[0]) + "_" + str(splitted2) + "n")##wild type
									temp_dict[splitted[0].lower()].append(str(position[0]))
									
							else:
								dict_mutations[splitted[0].lower()].append(str(position[0]) + "_" + str(splitted2) + "n")
								temp_dict[splitted[0].lower()].append(str(position[0]))
								
											

					elif splitted[1][0] == "r":
						position = re.findall("(\d+)", splitted[1])
						splitted2 = (each.split("\t")[1]).split("->")[0].replace(" ", "")
						splitted3 = (each.split("\t")[1]).split("->")[1].replace(" ", "")
						
						if "ins" not in each and "del" not in each and "delins" not in each:
							#print(each)
							dict_mutations[splitted[0].lower()].append(str(position[0]) + "_" + str(splitted2) + "r")##wild type
							temp_dict[splitted[0].lower()].append(str(position[0]) + "_" + str(splitted2) + "_" + str(splitted3)+"r")	

						else:
							if "del" in each:
								#print(each)
								if len(position) > 1:
									for l in range(int(position[0]), int(position[1])+1):
										dict_mutations[splitted[0].lower()].append(str(l) + "_" + str(splitted2) + "r")##wild type
										temp_dict[splitted[0].lower()].append(str(l))
								else:
									dict_mutations[splitted[0].lower()].append(str(position[0]) + "_" + str(splitted2) + "r")##wild type
									temp_dict[splitted[0].lower()].append(str(position[0]))
									
							else:
								dict_mutations[splitted[0].lower()].append(str(position[0]) + "_" + str(splitted2) + "r")
								temp_dict[splitted[0].lower()].append(str(position[0]))
								
											
					else:
						print("unexpected mutation", each)
								
									
			all_samples[item].append(temp_dict)

	results = open("%s_data_x.txt" % outdir, "w")

	##create a new dictionary to avoid repeated mutations
	dict_mutations2 = collections.defaultdict(list)

	total = []
	for each in dict_mutations:
		uniq_res = list(set(list(dict_mutations[each])))
		dict_mutations2[each].append(uniq_res)
		for i in dict_mutations[each]:
			total.append(i)

	##only consider the genes found associated with an antibiotic 
	isoniazid = ["katg_promoter_size_107bp", "katg", "inha", "kasa", "ahpc_promoter_size_180bp", "ahpc", "fabg1_promoter_size_223bp"]
	rifampicin = ["rpob", "rpoc"] 
	streptomycin = ["rpsl", "gidb","rrs"]
	ethambutol = ["embb", "embc", "emba_promoter_size_115bp", "embr", "ubia", "emba"]
	pyrazinamide = ["pnca_promoter_size_107bp", "pand", "rpsa", "pnca"]
	list_antibiotics = [ethambutol,pyrazinamide,streptomycin,rifampicin,isoniazid, dict_mutations2]

	all_aa = list(set(all_aa))

	for k in all_samples:
		results.write(str(k))
		results.write("\t")
		for g in dict_mutations2:
			if g in list(all_samples[k][0]):
				for each in dict_mutations2[g][0]:
					if each.split("_")[0] in [i.split("_")[0] for i in all_samples[k][0][g]]:
						index_list = []
						for x in all_samples[k][0][g]:
							index_list.append(x.split("_")[0])
						target_aa = index_list.index(each.split("_")[0])
						aa = all_samples[k][0][g][target_aa]
						if each[-1] != "n" and each[-1] != "r":
							if len(aa.split("_")) > 2:
								mutation = aa.split("_")[2]
								wild = aa.split("_")[1]
								if (wild, mutation) in blosum:
									score = blosum[(str(wild), str(mutation))]
								elif (mutation, wild) in blosum:
									score =  blosum[(str(mutation), str(wild))]
								else:
									score = -5
							else: 
								score = -5

						elif each[-1] == "n":
							if len(aa.split("_")) > 2:
								mutation = aa.split("_")[2][0:-1]
								wild = aa.split("_")[1]
								if (wild.upper(), mutation.upper()) in nucleotide:
									score = nucleotide[(str(wild).upper(), str(mutation).upper())]
								elif (mutation.upper(), wild.upper()) in nucleotide:
									score = nucleotide[(str(mutation).upper(), str(wild).upper())]
								else:
									print("nucleotide")
									print(mutation, wild)
							else:					
								score = -5
						elif each[-1] == "r":
							if len(aa.split("_")) > 2:
								mutation = aa.split("_")[2][0:-1]
								wild = aa.split("_")[1]
								if (wild.upper(), mutation.upper()) in nucleotide:
									score = nucleotide[(str(wild).upper(), str(mutation).upper())]
								elif (mutation.upper(), wild.upper()) in nucleotide:
									score = nucleotide[(str(mutation).upper(), str(wild).upper())]
								else:
									print("RNA")
									print(mutation, wild)
							else:
								score = -5

						results.write(str(score))
						results.write("\t")
						for currentArgument, currentValue in arguments:
							if currentArgument in ("-m","--mutcol"):
								results.write(str(1))
								results.write("\t")

					else:
						if each[-1] != "n" and each[-1] != "r":
							if "*" != each.split("_")[1] and "?" != each.split("_")[1]:
								score = blosum[each.split("_")[1], each.split("_")[1]]
							else:
								print(each)
								score = -5
							results.write(str(score))

						elif each[-1] == "n":
							score = 1
							results.write(str(score))
						elif each[-1] == "r":
							score = 1
							results.write(str(score))

						if each.split("_")[1][0:-1].upper() == "N":
							print("check that again")

						results.write("\t")
						for currentArgument, currentValue in arguments:
							if currentArgument in ("-m","--mutcol"):
								results.write(str(-1))
								results.write("\t")
			else:
				if g in dict_mutations2:
					for f in dict_mutations2[g][0]:
						if f[-1] != "r" and f[-1] != "n":
							if "*" != f.split("_")[1] and "?" != f.split("_")[1]:
								score = blosum[f.split("_")[1], f.split("_")[1]]
							else:
								print(f.split("_")[1])
								score = -5
							results.write(str(score))
						elif f[-1] == "r":
							score = 1
							results.write(str(score))
						elif f[-1] == "n":
							score = 1
							results.write(str(score))
						results.write("\t")
						for currentArgument, currentValue in arguments:
							if currentArgument in ("-m","--mutcol"):
								results.write(str(-1))
								results.write("\t")
		results.write("\n")
		
	results.close()

	##read the created file

	data_x = np.loadtxt("%s_data_x.txt" % outdir, dtype="string")

	data_y = np.loadtxt("%s" % y_path, dtype="string")

	data_y_list = [] 

	for each in data_y:
		data_y_list.append(each[0])

	data_x_new = open("%s_data_x_ordered.txt" % outdir, "w")
	data_y_new = open("%s_data_y_ordered.txt" % outdir, "w")
	for each in data_x[0:]:
		if each[0] in data_y_list:
			ind = data_y_list.index(each[0])
			for i in each[1:]:
				data_x_new.write(i)
				data_x_new.write("\t")
			data_x_new.write("\n")
			data_y_new.write(data_y[ind,1])
			data_y_new.write("\n")


	data_x_new.close()
	data_y_new.close()

	##this function takes the data and predictions and perform the random forest model 
	##Number of CV splits is optional
	##Random state is optional 
	
	##example data
	##the data structure is rows are samples, columns are mutations. 
	##The data entries could be binary such as absence/presence of the mutations and indels or could be Blosum and substitution matrix entries. 

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-s", "--split"):
			cv_split = int(currentValue)
		if currentArgument in ("-r", "--random"):
			Random_State = int(currentValue)

	data_x = np.loadtxt("%s_data_x_ordered.txt" % outdir, dtype = "float")
	data_y = np.loadtxt("%s_data_y_ordered.txt" % outdir)

	##seperate a validation data from the whole dataset
	##generate ramdom traning and validation data indexes
	all_samples = range(0, len(data_y)) ##all the available samples
	validation = range(0,len(data_y),5) ##the samples separated for the validation purpose 
	remain = list(set(all_samples) - set(validation)) ##the remaining samples will be used to train and test the model. 

	##validation data
	val_x = data_x[validation]
	val_y = data_y[validation]
	##test and training data
	data_x = data_x[remain]
	data_y = data_y[remain]

	##Look into the data
	##number of features 
	feat_num = len(data_x[0])
	##number of remaining training and test samples 
	sample_num = len(data_x)
	print("number of features %i" % feat_num)
	print("number of training samples %i" % sample_num)


	def combine_rfs(rf_a, rf_b):
	    ##this fuction merges the RF estimators
	    rf_a.estimators_ += rf_b.estimators_
	    rf_a.n_estimators = len(rf_a.estimators_)
	    return rf_a


	###cross validation 
	##split the data into the test and training data 
	skf = StratifiedKFold(n_splits=cv_split, random_state=Random_State, shuffle=True)
	skf.get_n_splits(data_x, data_y)


	##prepare the data stores 
	plt.figure(figsize=(8, 8))
	tprs = []
	aucs = []
	rf_all = []
	mean_fpr = np.linspace(0, 1, 100)	
	mcc_thresholds = []
	
	##normalization of the training data 
	for train_index, test_index in skf.split(data_x, data_y):
		x_train, x_test = data_x[train_index], data_x[test_index]
		y_train, y_test = data_y[train_index], data_y[test_index]

		for currentArgument, currentValue in arguments:
			if currentArgument in ("-n", "--normal"):
				from sklearn import preprocessing
				scaler = preprocessing.StandardScaler().fit(x_train)
				x_train = scaler.transform(x_train)
				x_test = scaler.transform(x_test) ##scale the test data by the training data 

		
		##random forest implementation
		clf_1 = RandomForestClassifier(n_estimators=100, random_state=Random_State)
		clf_1.fit(x_train, y_train) ##train the classifier
		mcc_all = []
		##Predict the test data results for each thresholds
		for each in np.arange(0, 1.1, 0.1):
			predict_final = []
			predict = (clf_1.predict_proba(x_test)[:,1] >= each).astype(bool)
			for item in predict:
				if item == False:
					predict_final.append(0.0)
				elif item == True:
					predict_final.append(1.0)
			
			######calculate mcc value#########
			mcc = matthews_corrcoef(y_test,predict_final)
			mcc_all.append(mcc)
		mcc_thresholds.append(mcc_all) ## Keep the MCC results
		rf_all.append(clf_1) ##Keep the RF classifier
		pred_y = clf_1.predict(x_test) 
	
		random_forest_test = (clf_1.predict(x_test)) ##Random Forest prediction results
		random_forest_prob = clf_1.predict_proba(x_test)[:,1] ##Random Forest prediction probability results
		fpr = dict()
		tpr = dict()
		roc_auc = dict()

		# Compute micro-average ROC curve and ROC area
		fpr, tpr, _ = roc_curve(y_test, random_forest_prob, pos_label = 1)
		tprs.append(interp(mean_fpr, fpr, tpr))
		tprs[-1][0] = 0.0
		roc_auc = auc(fpr, tpr)
		aucs.append(roc_auc)
		lw = 1
		plt.plot(fpr, tpr, color='darkorange', alpha = 0.3, lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
	plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r',label='Chance', alpha=.8) ##Plot the randomness 

	##Calculate the mean auc performance of test datasets
	mean_tpr = np.mean(tprs, axis=0)
	mean_tpr[-1] = 1.0
	mean_auc = auc(mean_fpr, mean_tpr)
	std_auc = np.std(aucs)
	plt.plot(mean_fpr, mean_tpr, color='b',label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc), lw=2, alpha=.8)

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-n", "--normal"):
			from sklearn import preprocessing
			scaler2 = preprocessing.StandardScaler().fit(data_x)
			val_x = scaler2.transform(val_x) ##scale the validation data by the training data 


	##Combine all the classifiers to predict the validation data
	rf_combined = reduce(combine_rfs, rf_all)
	random_forest_test1 = (rf_combined.predict(val_x)) ##validation data predictions
	random_forest_prob1 = rf_combined.predict_proba(val_x)[:,1] ##validation data prediction probabilities 

	##Calculate MCC performances of the validation data for all thresholds 
	mcc_all1 = []
	for e in np.arange(0, 1.1, 0.1):
		predict_final = []
		predict = (rf_combined.predict_proba(val_x)[:,1] >= e).astype(bool)
		for item in predict:
			if item == False:
				predict_final.append(0.0)
			elif item == True:
				predict_final.append(1.0)

		######calculate mcc value#########
		mcc = matthews_corrcoef(val_y,predict_final)
		mcc_all1.append(mcc)		
	
	##Compute micro-average ROC curve and ROC area for the validation data
	fpr1, tpr1, _ = roc_curve(val_y, random_forest_prob1, pos_label = 1)
	roc_auc1 = auc(fpr1, tpr1)
	lw = 1
	plt.plot(fpr1, tpr1, color='red', alpha = 1, lw=lw, label='ROC curve for validation (area = %0.2f)' % roc_auc1)	

	##Plot AUC performances for the test and validation data 
	std_tpr = np.std(tprs, axis=0)
	tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
	tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
	plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2, label=r'$\pm$ 1 std. dev.')

	plt.xlim([-0.05, 1.05])
	plt.ylim([-0.05, 1.05])
	plt.xlabel('False Positive Rate')
	plt.ylabel('True Positive Rate')
	plt.title('ROC - the data includes %s features' % str(feat_num))
	plt.legend(loc="lower right")
	for currentArgument, currentValue in arguments:
		if currentArgument in ("-k", "--keep"):
			plt.savefig("./AUC_ROC_plot.png")
	plt.show()
	##Take the average of MCC values 
	sum_all_mcc = np.sum(mcc_thresholds, axis = 0)
	aver_all_mcc = []
	for val in sum_all_mcc:
		aver_all_mcc.append(val/cv_split)


	##Plot the MCC values for the test and the validation data
	plt.plot(np.arange(0,1.1,0.1),aver_all_mcc, color = "darkred", alpha = 1, label = "Test data")
	plt.plot(np.arange(0,1.1,0.1),mcc_all1, color = "darkblue", alpha = 1, label = "Validation data")
	plt.xlim([0,1])
	plt.xlabel("Thresholds")
	plt.ylabel("MCC average values for %s fold CV" %str(cv_split))
	plt.title("Thresholds vs MCC values")
	plt.legend(loc="best")
	for currentArgument, currentValue in arguments:
		if currentArgument in ("-k", "--keep"):
			plt.savefig("./MCC_plot.png")
	plt.show()


if __name__ == '__main__':
    main()		
	





	
