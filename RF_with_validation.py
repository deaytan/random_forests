#!/usr/bin/python2.7

import getopt, sys
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]
#print(argumentList)

unixOptions = "x:y:s:r:nkh"
gnuOptions = ["xdata=", "ydata=", "split=", "random=", "normal", "keep","help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-x --xdata = input x data")
		print("-y --ydata = input y data") 
		print("-s --split = number of CV splits")
		print("-r --random = random state")
		print("-n --normal = normalize the data")
		print("-k --keep = keep the generated ROC and MCC plots")
		print("-h --help = show the help message (have fun!))")
		sys.exit()


def main():

	##this function takes the data and predictions and perform the random forest model 
	##Number of CV splits is optional
	##Random state is optional 
	
	import numpy as np
	import random
	from sklearn.ensemble import RandomForestClassifier
	from sklearn.model_selection import StratifiedKFold
	from sklearn.metrics import matthews_corrcoef
	import matplotlib.pyplot as plt
	from sklearn.metrics import roc_curve, auc
	from scipy import interp
	
	##example data
	##the data structure is rows are samples, columns are mutations. 
	##The data entries could be binary such as absence/presence of the mutations and indels or could be Blosum and substitution matrix entries. 

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-x","--xdata"):
			data_x = np.loadtxt("./%s" % currentValue, dtype="float")
		if currentArgument in ("-y","--ydata"):
			data_y = np.loadtxt("./%s" % currentValue)
		if currentArgument in ("-s", "--split"):
			cv_split = int(currentValue)
		if currentArgument in ("-r", "--random"):
			Random_State = int(currentValue)



	##seperate a validation data from the whole dataset
	##generate ramdom traning and validation data indexes
	all_samples = range(0, len(data_y)) ##all the available samples
	validation = range(0,len(data_y),5) ##the samples separated for the validation purpose 
	remain = list(set(all_samples) - set(validation)) ##the remaining samples will be used to train and test the model. 

	##validation data
	val_x = data_x[validation]
	val_y = data_y[validation]
	##test and training data
	data_x = data_x[remain]
	data_y = data_y[remain]

	##Look into the data
	##number of features 
	feat_num = len(data_x[0])
	##number of remaining training and test samples 
	sample_num = len(data_x)
	print("number of features %i" % feat_num)
	print("number of training samples %i" % sample_num)


	def combine_rfs(rf_a, rf_b):
	    ##this fuction merges the RF estimators
	    rf_a.estimators_ += rf_b.estimators_
	    rf_a.n_estimators = len(rf_a.estimators_)
	    return rf_a


	###cross validation 
	##split the data into the test and training data 
	skf = StratifiedKFold(n_splits=cv_split, random_state=Random_State, shuffle=True)
	skf.get_n_splits(data_x, data_y)


	##prepare the data stores 
	plt.figure(figsize=(8, 8))
	tprs = []
	aucs = []
	rf_all = []
	mean_fpr = np.linspace(0, 1, 100)	
	mcc_thresholds = []
	
	##normalization of the training data 
	for train_index, test_index in skf.split(data_x, data_y):
		x_train, x_test = data_x[train_index], data_x[test_index]
		y_train, y_test = data_y[train_index], data_y[test_index]

		for currentArgument, currentValue in arguments:
			if currentArgument in ("-n", "--normal"):
				from sklearn import preprocessing
				scaler = preprocessing.StandardScaler().fit(x_train)
				x_train = scaler.transform(x_train)
				x_test = scaler.transform(x_test) ##scale the test data by the training data 

		
		##random forest implementation
		clf_1 = RandomForestClassifier(n_estimators=100, random_state=Random_State)
		clf_1.fit(x_train, y_train) ##train the classifier
		mcc_all = []
		##Predict the test data results for each thresholds
		for each in np.arange(0, 1.1, 0.1):
			predict_final = []
			predict = (clf_1.predict_proba(x_test)[:,1] >= each).astype(bool)
			for item in predict:
				if item == False:
					predict_final.append(0.0)
				elif item == True:
					predict_final.append(1.0)
			
			######calculate mcc value#########
			mcc = matthews_corrcoef(y_test,predict_final)
			mcc_all.append(mcc)
		mcc_thresholds.append(mcc_all) ## Keep the MCC results
		rf_all.append(clf_1) ##Keep the RF classifier
		pred_y = clf_1.predict(x_test) 
	
		random_forest_test = (clf_1.predict(x_test)) ##Random Forest prediction results
		random_forest_prob = clf_1.predict_proba(x_test)[:,1] ##Random Forest prediction probability results
		fpr = dict()
		tpr = dict()
		roc_auc = dict()

		# Compute micro-average ROC curve and ROC area
		fpr, tpr, _ = roc_curve(y_test, random_forest_prob, pos_label = 1)
		tprs.append(interp(mean_fpr, fpr, tpr))
		tprs[-1][0] = 0.0
		roc_auc = auc(fpr, tpr)
		aucs.append(roc_auc)
		lw = 1
		plt.plot(fpr, tpr, color='darkorange', alpha = 0.3, lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
	plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r',label='Chance', alpha=.8) ##Plot the randomness 

	##Calculate the mean auc performance of test datasets
	mean_tpr = np.mean(tprs, axis=0)
	mean_tpr[-1] = 1.0
	mean_auc = auc(mean_fpr, mean_tpr)
	std_auc = np.std(aucs)
	plt.plot(mean_fpr, mean_tpr, color='b',label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc), lw=2, alpha=.8)

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-n", "--normal"):
			from sklearn import preprocessing
			scaler2 = preprocessing.StandardScaler().fit(data_x)
			val_x = scaler2.transform(val_x) ##scale the validation data by the training data 


	##Combine all the classifiers to predict the validation data
	rf_combined = reduce(combine_rfs, rf_all)
	random_forest_test1 = (rf_combined.predict(val_x)) ##validation data predictions
	random_forest_prob1 = rf_combined.predict_proba(val_x)[:,1] ##validation data prediction probabilities 

	##Calculate MCC performances of the validation data for all thresholds 
	mcc_all1 = []
	for e in np.arange(0, 1.1, 0.1):
		predict_final = []
		predict = (rf_combined.predict_proba(val_x)[:,1] >= e).astype(bool)
		for item in predict:
			if item == False:
				predict_final.append(0.0)
			elif item == True:
				predict_final.append(1.0)

		######calculate mcc value#########
		mcc = matthews_corrcoef(val_y,predict_final)
		mcc_all1.append(mcc)		
	
	##Compute micro-average ROC curve and ROC area for the validation data
	fpr1, tpr1, _ = roc_curve(val_y, random_forest_prob1, pos_label = 1)
	roc_auc1 = auc(fpr1, tpr1)
	lw = 1
	plt.plot(fpr1, tpr1, color='red', alpha = 1, lw=lw, label='ROC curve for validation (area = %0.2f)' % roc_auc1)	

	##Plot AUC performances for the test and validation data 
	std_tpr = np.std(tprs, axis=0)
	tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
	tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
	plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2, label=r'$\pm$ 1 std. dev.')

	plt.xlim([-0.05, 1.05])
	plt.ylim([-0.05, 1.05])
	plt.xlabel('False Positive Rate')
	plt.ylabel('True Positive Rate')
	plt.title('ROC - the data includes %s features' % str(feat_num))
	plt.legend(loc="lower right")
	for currentArgument, currentValue in arguments:
		if currentArgument in ("-k", "--keep"):
			plt.savefig("./AUC_ROC_plot.png")
	plt.show()
	##Take the average of MCC values 
	sum_all_mcc = np.sum(mcc_thresholds, axis = 0)
	aver_all_mcc = []
	for val in sum_all_mcc:
		aver_all_mcc.append(val/cv_split)


	##Plot the MCC values for the test and the validation data
	plt.plot(np.arange(0,1.1,0.1),aver_all_mcc, color = "darkred", alpha = 1, label = "Test data")
	plt.plot(np.arange(0,1.1,0.1),mcc_all1, color = "darkblue", alpha = 1, label = "Validation data")
	plt.xlim([0,1])
	plt.xlabel("Thresholds")
	plt.ylabel("MCC average values for %s fold CV" %str(cv_split))
	plt.title("Thresholds vs MCC values")
	plt.legend(loc="best")
	for currentArgument, currentValue in arguments:
		if currentArgument in ("-k", "--keep"):
			plt.savefig("./MCC_plot.png")
	plt.show()


if __name__ == '__main__':
    main()		
	


	