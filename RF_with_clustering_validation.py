#!/usr/bin/python2.7
import getopt, sys
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")


##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]
#print(argumentList)

unixOptions = "x:y:n:c:s:r:nh"
gnuOptions = ["xdata=", "ydata=", "names=", "clusters=", "split=", "random=", "normal","help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-x --xdata = input x data")
		print("-y --ydata = output y data") 
		print("-n --name = sample IDs")
		print("-c --clusters = sample clusters")
		print("-s --split = number of CV splits")
		print("-r --random = random state")
		print("-n --normal = normalize the data")
		print("-h --help = show the help message (have fun!))")
		sys.exit()


def main():

	##this function takes the data and predictions and perform the random forest model 
	##Number of CV splits is optional
	##Random state is optional 
	
	import numpy as np
	import random
	from sklearn.ensemble import RandomForestClassifier
	from sklearn.model_selection import StratifiedKFold
	from sklearn.metrics import matthews_corrcoef
	import matplotlib.pyplot as plt
	from sklearn.metrics import roc_curve, auc
	from scipy import interp
	import collections
	from sklearn import utils
	
	##example data
	##the data structure is rows are samples, columns are mutations. 
	##The data entries could be binary such as absence/presence of the mutations and indels or could be Blosum and substitution matrix entries. 

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-x","--xdata"):
			data_x = currentValue
		if currentArgument in ("-y","--ydata"):
			data_y = currentValue
		if currentArgument in ("-n","--name"):
			names = currentValue
		if currentArgument in ("-c","--clusters"):
			clusters = currentValue
		if currentArgument in ("-s", "--split"):
			cv_split = int(currentValue)
		if currentArgument in ("-r", "--random"):
			Random_State = int(currentValue)


	##cluster the samples
	output_file_open = open("%s" % clusters, "r")
	output_file = output_file_open.readlines()

	dict_cluster = collections.defaultdict(list)

	for each in output_file:
		splitted = each.split()
		if splitted != []:
			if splitted[0].isdigit():
				dict_cluster[splitted[0]].append(splitted[3])
			if splitted[0] == "Similar":
				splitted = each.split()
				splitted_2 = each.split(":")
				dict_cluster[splitted_2[1].split()[0]].append(splitted[6])

	keys = sorted(dict_cluster.keys())

	##upload the data
	data_x = np.loadtxt("%s" % data_x, dtype="float")
	data_y = np.loadtxt("%s" % data_y)
	names_open = open("%s" % names, "r")
	names_read = names_open.readlines()

	##find the sample names
	names_all = []
	for each in names_read:
		names_all.append(each.replace("\n", ""))

	##seperate a validation data 
	##random one sixth of the clusters are the validation clusters 
	desired_keys = range(1, len(keys)+1, 6)

	##validation indexes and names 			
	validation = []
	validation_named = []
	for each in desired_keys:
		for item in dict_cluster[str(each)]:
			if item in names_all:
				validation_named.append(item)
				validation.append(names_all.index(item))

	##training and testing data indexes
	all_samples = range(0, len(data_y))
	remain = list(set(all_samples) - set(validation))

	##validation data 
	val_x = data_x[validation]
	val_y = data_y[validation]

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-n", "--normal"):
			val_x_raw = val_x			

	##test and training data
	data_x = data_x[remain]
	data_y = data_y[remain]


	##information about the training/testing data
	feat_num = len(data_x[0])
	sample_num = len(data_x)
	print("number of features %i" % feat_num)
	print("number of samples %i" % sample_num)

	##subtract the samples from the names as well:
	names = []
	for e in range(0, len(names_all)):
		if e not in validation:
			names.append(names_all[e])	

	##Custom cross validation
	##it takes care cluster information
	all_data_splits = []
	cv = cv_split
	all_available_data = range(1,len(dict_cluster)+1)
	shuffled = list(utils.shuffle(keys, random_state = 42))

	r = int(len(shuffled)/cv)
	a = 0
	b = r
	for i in range(cv):
		all_data_splits.append(shuffled[a:b])
		
		if i != cv - 2:
			a = b
			b = b + r
		
		else:
			a = b
			b = len(shuffled)

	##prepare the data stores 
	plt.figure(figsize=(8, 8))
	tprs = []
	aucs = []
	rf_all = []
	mean_fpr = np.linspace(0, 1, 100)
	mcc_thresholds = []
	all_validation_results_prob = []
	all_validation_results_class = []
	for cc in range(cv):
			test_clusters = all_data_splits[cc]
			remain = list(set(range(cv)) - set([cc]))
			training_clusters = []
			for e in remain:
				for ee in all_data_splits[e]:
					training_clusters.append(ee)
			
			###create the training data:
			train_samples = []
			for each in training_clusters:
					for element in dict_cluster["%s" %each]:
						if element in names:
							train_samples.append(names.index(element))


			###create the test data:
			test_samples = []
			for each in test_clusters:
					for element in dict_cluster["%s" %each]:
						if element in names:
							test_samples.append(names.index(element))
			
			##train data		
			x_train = data_x[train_samples]
			y_train = data_y[train_samples]

			##test data
			x_test = data_x[test_samples]
			y_test = data_y[test_samples]

			for currentArgument, currentValue in arguments:
				if currentArgument in ("-n", "--normal"):
					from sklearn import preprocessing
					scaler = preprocessing.StandardScaler().fit(x_train)
					x_train = scaler.transform(x_train)
					x_test = scaler.transform(x_test) ##scale the test data by the training data 
					val_x = scaler.transform(val_x_raw) ##scale the validation data by the training data

			
			##random forest
			clf_1 = RandomForestClassifier(n_estimators=100, random_state=42)
			clf_1.fit(x_train, y_train)
			mcc_all = []
			for each in np.arange(0, 1.1, 0.1):
				predict_final = []
				predict = (clf_1.predict_proba(x_test)[:,1] >= each).astype(bool)
				for item in predict:
					if item == False:
						predict_final.append(0.0)
					elif item == True:
						predict_final.append(1.0)

				######calculate mcc value#########
				mcc = matthews_corrcoef(y_test,predict_final)
				mcc_all.append(mcc)
			mcc_thresholds.append(mcc_all)
			rf_all.append(clf_1)
			pred_y = clf_1.predict(x_test)
			
			random_forest_test = (clf_1.predict(x_test))
			random_forest_prob = clf_1.predict_proba(x_test)[:,1]

			##validation results 
			all_validation_results_class.append(clf_1.predict(val_x))
			all_validation_results_prob.append(clf_1.predict_proba(val_x)[:,1])

			fpr = dict()
			tpr = dict()
			roc_auc = dict()

			##Compute micro-average ROC curve and ROC area
			fpr, tpr, _ = roc_curve(y_test, random_forest_prob, pos_label = 1)
			tprs.append(interp(mean_fpr, fpr, tpr))
			tprs[-1][0] = 0.0
			roc_auc = auc(fpr, tpr)
			aucs.append(roc_auc)
			lw = 1
			plt.plot(fpr, tpr, color='darkorange', alpha = 0.3, lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
	plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r', label='Chance', alpha=.8)
	mean_tpr = np.mean(tprs, axis=0)
	mean_tpr[-1] = 1.0
	mean_auc = auc(mean_fpr, mean_tpr)
	std_auc = np.std(aucs)
	plt.plot(mean_fpr, mean_tpr, color='b', label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc), lw=2, alpha=.8)

	##take the average of the predictions 
	random_forest_prob1 = np.array(all_validation_results_prob).sum(axis = 0)/float(cv)

	mcc_all1 = []
	for e in np.arange(0, 1.1, 0.1):
		predict_final = []
		predict = (random_forest_prob1 >= e).astype(bool)
		for item in predict:
				if item == False:
					predict_final.append(0.0)
				elif item == True:
					predict_final.append(1.0)

		######calculate mcc value#########
		mcc = matthews_corrcoef(val_y,predict_final)
		mcc_all1.append(mcc)		
		
	#Compute micro-average ROC curve and ROC area
	fpr1, tpr1, _ = roc_curve(val_y, random_forest_prob1, pos_label = 1)
	roc_auc1 = auc(fpr1, tpr1)
	lw = 1
	plt.plot(fpr1, tpr1, color='red', alpha = 1, lw=lw, label='ROC curve for validation (area = %0.2f)' % roc_auc1)	
	std_tpr = np.std(tprs, axis=0)
	tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
	tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
	plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2, label=r'$\pm$ 1 std. dev.')
	plt.xlim([-0.05, 1.05])
	plt.ylim([-0.05, 1.05])
	plt.xlabel('False Positive Rate')
	plt.ylabel('True Positive Rate')
	plt.title('ROC')
	plt.legend(loc="lower right")
	plt.savefig("ROC_AUC_plot_RF.png") ##save the plot
	plt.close()

	##Generate the MCC plot
	sum_all_mcc = np.sum(mcc_thresholds, axis = 0)
	aver_all_mcc = []
	for val in sum_all_mcc:
			aver_all_mcc.append(val/float(cv))

	plt.plot(np.arange(0,1.1,0.1),aver_all_mcc, color = "darkred", alpha = 1, label = "Test data")
	plt.plot(np.arange(0,1.1,0.1),mcc_all1, color = "darkblue", alpha = 1, label = "Validation data")
	plt.xlim([0,1])
	plt.xlabel("Thresholds")
	plt.ylabel("MCC average values")
	plt.title("Thresholds vs MCC values - clustered genes and samples")
	plt.legend(loc="best")
	plt.savefig("MCC_plot_RF.png") ##save the plot
	plt.close()


if __name__ == '__main__':
    main()				
		