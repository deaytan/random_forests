# Random Forests

---

In the repository there are several Python scripts. 

# RF_with_validation.py 

The first Python script called RF_with_validation.py takes the custom input and true output data to predict outputs using Random Forest.
The program provides two different plots to evaluate prediction results. 

### Program options
-x --xdata = input x data 

-y --ydata = input y data

-s --split = number of CV splits

-r --random = random state

-n --normal = normalize the data

-k --keep = keep the generated ROC and MCC plots

-h --help = show the help message (have fun!)

### Run the program

In order to run the RF_with_validation.py please follow the instructions:

Type on the command line:

python RF_with_validation.py -x /path/to/input_x -y /path/to/input_y -s [number] -r [number] [options]


---

# analyse_pointfinder_with_ML.py

The second script takes the Pointfinder tool .tsv results. The output file includes all the detected mutations by PointFinder. 
This input file is generated based on Blosum62 and nucleotide substitution matrix. All the obtained point mutations and indels
are scored with these matrices. 

This script does not take into consideration resfinder results.

The cross validation process is done without taking into account clusters.

The program uses the generated output as an input to predict resistance profiles (might be something else as well) using random forest. To evaluate the prediction performances, the program provides two different plots ROC-AUC and MCC. 

Plase note, the pointfinder folder names should be identical with the .tsv files. If the file name is 2345, the tsv file would be 2345_kma_results.tsv. Otherwise, the program will give errors or wrong results. 

### Program options

-p --path = path to the input directory

-a --antibiotic = pick an antibiotic or select all genes

the options: ethambutol, pyrazinamide, streptomycin, rifampicin, isoniazid, all

Hint, if the analysis is not related to these specified antibiotics or all the genes should be used please select all

-o --output = give a name for the output

-y --ydata = path to the true outputs

-s --split = number of CV splits

-r --random = random state

-n --normal = normalize the data

-k --keep = keep the generated ROC and MCC plots

-h --help = print the help message (have fun!)

### Run the program

In order to run the analyse_pointfinder_with_ML.py please follow the instructions:

Type on the command line:

python analyse_pointfinder_with_ML.py -p /path/to/tsv -o output_name -a antibiotic_name -y /path/to/true/output -s [number] -r [number] [options]

---

# RF_with_clustering_validation.py

This script predicts profiles using random forests and takes into account clustering in the cross-validation process different than RF_with_validation.py and analyse_pointfinder_with_ML.py. 

The clustering process should be completed using kma_clustering program (available in the data_preparation folder). 

### Program options

-x --xdata = input x data

-y --ydata = output y data

-n --name = sample IDs

-c --clusters = sample clusters

-s --split = number of CV splits

-r --random = random state

-n --normal = normalize the data

-h --help = show the help message (have fun!)

### Running the program

python RF_with_clustering_validation.py -x /path/to/input/data -y /path/to/output/data -n /path/to/sample/names -c /path/to/sample/clusters -s [number] -r [number] [options]

### Example Data

The Data_TB folder includes example PointFinder .tsv outputs. The ETHAMBUTOL_tb.txt file represents the format of the single-output data. 

# Dependencies

## Required Python Packages

* getopt
* sys
* warnings
* matplotlib
* numpy
* sklearn
* scipy
* collections
* random